#!/bin/bash
cd $(git rev-parse --show-toplevel)
source venv/bin/activate
python3 -m autopep8 --aggressive --aggressive --in-place --recursive --verbose ./app/
git status
echo ""
echo "===================================================="
echo "MAKE SURE TO RUN 'git add .' BEFORE COMMITTING AGAIN"
echo "===================================================="
echo ""
