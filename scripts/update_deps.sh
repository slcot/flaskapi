#!/bin/bash
cd $(git rev-parse --show-toplevel)
source venv/bin/activate
pip3 freeze | grep -v "pkg-resources" > requirements.txt

