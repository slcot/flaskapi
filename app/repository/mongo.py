
import os
import mongoengine


class MongoRepository(object):

    DB_NAME = "main_db"

    result = None

    def __init__(self):
        mongo_host = os.environ.get('MONGO_HOST')
        mongo_port = os.environ.get('MONGO_PORT')
        mongo_username = os.environ.get('MONGO_USERNAME')
        mongo_password = os.environ.get('MONGO_PASSWORD')

        assert mongo_host != ""
        assert mongo_port != ""
        assert mongo_username != ""
        assert mongo_password != ""

        MongoRepository.result = mongoengine.connect(db=MongoRepository.DB_NAME,
                                                     host=mongo_host,
                                                     port=int(mongo_port),
                                                     username=mongo_username,
                                                     password=mongo_password,
                                                     authentication_source='admin')
