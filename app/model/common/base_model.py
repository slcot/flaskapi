
from mongoengine import (
    Document,
    UUIDField,
    StringField,
    DateTimeField
)

from app.model.common.validators import validate_version_number


class BaseModel(Document):
    """ Base Mongo validation model """

    uuid = UUIDField(required=True,
                     primary_key=True)

    schema_version = StringField(default='0.0.1',
                                 validation=validate_version_number('Schema version'))

    created_at = DateTimeField(required=True)

    meta = {
        'allow_inheritance': True,
        'indexes': [
            {'fields': ['-uuid'], 'unique': True},
        ]
    }
