
import string
import numpy as np


class PropNotFoundError(Exception):
    def __init__(self, prop):
        self.message = "{} was expected but not specified."


def apply_all(functions, keys, dictionary, strict=True):
    for key in keys:
        for f in functions:
            if key not in dictionary:
                if not strict:
                    continue
                raise PropNotFoundError(key)
            dictionary[key] = f(dictionary[key])


def generate_confirm_code():
    return "".join(list(np.random.choice(
        list(string.ascii_lowercase + string.digits), 6)))
