
import re
from mongoengine import ValidationError


# Raise a validation exception of regex is not matched by input
def _raise_if_not_match(pattern, input, error_msg):
    if not re.match(pattern, input):
        raise ValidationError(error_msg)

# Return a function that when passed an input will check validation


def _wrap_validator(pattern, error_msg):
    return lambda s: _raise_if_not_match(pattern, s, error_msg)

# Validates against letters and numbers


def validate_alphanumeric(field_name):
    return _wrap_validator(r'^[0-9a-zA-Z]+$',
                           error_msg='{} can only contain characters and numbers'.format(field_name))


# Validates against letters and numbers and underscores
def validate_alphanumeric_underscore(field_name):
    return _wrap_validator(r'^[0-9a-zA-Z_]+$',
                           error_msg='{} can only contain characters, numbers, and underscores'.format(field_name))


# Validates against letters, hyphens and spaces
def validate_name(field_name):
    return _wrap_validator(r'^[a-zA-Z- ]+$',
                           error_msg='{} can only contain characters, hyphens, and non-leading/trailing spaces'.format(field_name))


# Validates against SHA256 hashses
def validate_SHA256(field_name):
    return _wrap_validator('^[0-9a-fA-F]{64}$',
                           error_msg='{} is not a SHA256 hash'.format(field_name))


# Validates against version numbers sucht as 1.10.2
def validate_version_number(field_name):
    return _wrap_validator('^[0-9]{1,3}[.][0-9]{1,2}[.][0-9]{1,2}$',
                           error_msg='{} is not a valid version number.'.format(field_name))
