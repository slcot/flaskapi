
from datetime import datetime
from uuid import uuid4
import json

from app.model.common.utils import generate_confirm_code
from app.model.user.user_model import UserModel


class UserService(object):

    @staticmethod
    def find_all(**kwargs):
        users = UserModel.objects(**kwargs)
        return [UserService.dump(user) for user in users]

    @staticmethod
    def find(uuid):
        user = UserModel.objects(uuid=uuid).first()
        if user is None:
            return {}
        return UserService.dump(user)

    @staticmethod
    def create(input_json):
        input_json['created_at'] = str(datetime.now())
        input_json['confirm_code'] = generate_confirm_code()
        input_json['uuid'] = str(uuid4())

        UserModel.sanitize_(input_json)
        user = UserModel(**input_json)
        user.validate()
        user.save()
        output_json = UserService.dump(user)

        return output_json

    @staticmethod
    def update(uuid, input_json):
        user = UserModel.objects(uuid=uuid).first()
        if user is None:
            return {}

        UserModel.sanitize_(input_json, strict=False)

        for key, value in input_json.items():
            setattr(user, key, value)

        user.validate()
        user.save()

        output_json = UserService.dump(user)

        return output_json

    @staticmethod
    def delete(uuid):
        user = UserModel.objects(uuid=uuid).first()
        if user is None:
            return {}
        user.delete()

        return {'success': 'true'}

    @staticmethod
    def dump(user):
        output_json = json.loads(user.to_json())
        output_json['uuid'] = str(user.uuid)
        if user.last_login is None:
            last_login = "null"
        else:
            last_login = str(user.last_login)
        output_json['created_at'] = str(user.created_at)
        output_json['last_login'] = last_login
        del output_json['_id']
        del output_json['_cls']
        del output_json['password']

        return output_json
