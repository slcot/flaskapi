
from mongoengine import (
    StringField,
    EmailField,
    BooleanField,
    DateTimeField
)

from app.model.common.base_model import BaseModel
from app.model.common.utils import apply_all
from app.model.common.validators import (
    validate_alphanumeric,
    validate_alphanumeric_underscore,
    validate_name,
    validate_SHA256,
    validate_version_number
)


class UserModel(BaseModel):

    first_name = StringField(required=True, min_length=2, max_length=50,
                             validation=validate_name('First name'))

    last_name = StringField(required=True, min_length=2, max_length=50,
                            validation=validate_name('Last name'))

    username = StringField(required=True, min_length=2, max_length=22,
                           validation=validate_alphanumeric_underscore('Username'))

    password = StringField(required=True,
                           validation=validate_SHA256('Password'))

    email = EmailField(required=True, max_length=80)

    confirm_code = StringField(required=True,
                               validation=validate_alphanumeric('Confirm code'))

    confirmed = BooleanField(default=False)

    last_login = DateTimeField(null=True)

    meta = {
        'indexes': [
            {'fields': ['-username', '-email'], 'unique': True},
        ]
    }

    @staticmethod
    def sanitize_(json_input, strict=True):
        invalids = [key for key in json_input if not hasattr(UserModel, key)]
        for key in invalids:
            del json_input[key]

        funcs = [lambda s: s.lower().strip()]
        keys = [
            'username',
            'password',
            'email',
            'confirm_code'
        ]
        apply_all(funcs, keys, json_input, strict)

        funcs = [lambda s: s.strip()]
        keys = [
            'first_name',
            'last_name'
        ]

        apply_all(funcs, keys, json_input, strict)
