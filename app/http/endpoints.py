
from flask import Flask, json, g, request
from flask_cors import CORS
import logging

from app.repository.mongo import MongoRepository
from app.model.user.user_service import UserService


mongo = MongoRepository()
app = Flask(__name__)
CORS(app)


@app.route("/users", methods=["GET"])
def index():
    users = UserService.find_all()
    return json_response(users)


@app.route("/users", methods=["POST"])
def create():
    json_data = json.loads(request.data)
    user = UserService.create(json_data)
    return json_response(user)


@app.route("/user/<string:uuid>", methods=["GET"])
def show(uuid):
    user = UserService.find(uuid)

    if user:
        return json_response(user)
    else:
        return json_response({'error': 'user not found'}, 404)


@app.route("/user/<string:uuid>", methods=["PUT"])
def update(uuid):
    json_data = json.loads(request.data)
    user = UserService.update(uuid, json_data)
    if user:
        return json_response(user)
    else:
        return json_response({'error': 'user not found'}, 404)


@app.route("/user/<string:uuid>", methods=["DELETE"])
def delete(uuid):
    if UserService.delete(uuid):
        return json_response({})
    else:
        return json_response({'error': 'user not found'}, 404)


def json_response(payload, status=200):
    return (json.dumps(payload), status, {'content-type': 'application/json'})
