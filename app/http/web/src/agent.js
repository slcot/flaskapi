import axios from 'axios';

const API_ROOT = 'https://conduit.productionready.io/api';

const responseBody = (res) => res.body;

/* eslint-disable no-unused-vars */
let token = null;

// TODO: Token handling
// https://medium.com/swlh/handling-access-and-refresh-tokens-using-axios-interceptors-3970b601a5da

const requests = {
  del: (url) => axios.delete(`${API_ROOT}${url}`).then(responseBody),
  get: (url) => axios.get(`${API_ROOT}${url}`).then(responseBody),
  put: (url, body) => axios.put(`${API_ROOT}${url}`, body).then(responseBody),
  post: (url, body) => axios.post(`${API_ROOT}${url}`, body).then(responseBody),
};

const Auth = {
  current: () => requests.get('/user'),
  login: (email, password) => requests.post('/users/login', { user: { email, password } }),
  register: (username, email, password) =>
    requests.post('/users', { user: { username, email, password } }),
  save: (user) => requests.put('/user', { user }),
};

const Profile = {
  get: (username) => requests.get(`/profiles/${username}`),
};

export default {
  Auth,
  Profile,
  setToken: (_token) => {
    token = _token;
  },
};
