import { PROFILE_PAGE_LOADED, PROFILE_PAGE_UNLOADED } from '../actions/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case PROFILE_PAGE_LOADED:
      return {};
    case PROFILE_PAGE_UNLOADED:
      return {};
    default:
      return state;
  }
};
