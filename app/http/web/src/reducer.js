import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import auth from './reducers/auth';
import common from './reducers/common';
import home from './reducers/home';
import profile from './reducers/profile';
import settings from './reducers/settings';

const createRootReducer = (history) =>
  combineReducers({
    auth,
    common,
    home,
    profile,
    settings,
    router: connectRouter(history),
  });

export default createRootReducer;
