import React from 'react';

const NoMatch = () => {
  return (
    <div>
      <p> This page does not exist. </p>
    </div>
  );
};

export default NoMatch;
