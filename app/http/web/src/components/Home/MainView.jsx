import React from 'react';
import { connect } from 'react-redux';
import { CHANGE_TAB } from '../../actions/actionTypes';

const mapStateToProps = (state) => ({
  token: state.common.token,
});

const mapDispatchToProps = (dispatch) => ({
  onTabClick: (tab, pager, payload) => dispatch({ type: CHANGE_TAB, tab, pager, payload }),
});

const MainView = () => {
  return (
    <div className="col-md-9">
      <div className="feed-toggle">
        <p>This is the home page</p>
        <ul className="nav nav-pills outline-active" />
      </div>
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(MainView);
