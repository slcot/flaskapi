# flaskapi

### Install

To install libraries and git hooks, run `./install.sh`.

### Debug

To debug, run `./debug.sh`. This will spin up a new or exist MongoDB and Flask docker container network.

### Purge

To purge the database and docker containers, run `./purge.sh`.

### Linting

Before code is pushed, it is automatically linted. If the lint comes back with errors, run `./lintfix.sh`, git add files and then try again.

