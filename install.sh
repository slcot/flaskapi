#!/bin/bash
DIR=$(git rev-parse --show-toplevel)

VENV_DIR=$DIR/venv
if [ ! -d "$VENV_DIR" ]; then
    virtualenv --python=python3.6 venv
fi

source venv/bin/activate

pip3 install -r requirements.txt

./scripts/install_hooks.sh


if [ -z "$(which docker)" ]
then
    curl -fsSL https://get.docker.com -o get-docker.sh
    sudo sh get-docker.sh
    rm get-docker.sh
    sudo usermod -aG docker $USER
fi

if [ -z "$(which docker-compose)" ]
then
    sudo curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
fi

