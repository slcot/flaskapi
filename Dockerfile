FROM python:3.6
ADD . /code
WORKDIR /code
RUN pip3 install -r requirements.txt
ENV FLASK_APP=/code/app/http/endpoints.py
ENV FLASK_ENV=development
ENV FLASK_RUN_HOST=0.0.0.0
ENV MONGO_HOST=mongo_service
ENV MONGO_PORT=27017
ENV MONGO_USERNAME=admin
ENV MONGO_PASSWORD=admin
CMD python3 -m flask run --port 8080

